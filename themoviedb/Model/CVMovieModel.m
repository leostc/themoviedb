//
//  CVMovieModel.m
//  themoviedb
//
//  Created by HaoLi on 28/03/2018.
//  Copyright © 2018 curve. All rights reserved.
//

#import "CVMovieModel.h"

@interface CVMovieModel()

@end

@implementation CVMovieModel

+ (NSString *)primaryKey {
    return @"ID";
}

+ (NSDictionary *)defaultPropertyValues{
    return @{@"isFav":@(NO)};
}

+ (NSArray<NSString *> *)ignoredProperties {
    return @[];
}

+ (NSArray *)requiredProperties {
    return @[];
}

+ (NSArray *)indexedProperties {
    return @[@"ID"];
}

@end
