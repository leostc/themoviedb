//
//  CVMovieModel.h
//  themoviedb
//
//  Created by HaoLi on 28/03/2018.
//  Copyright © 2018 curve. All rights reserved.
//

#import <Realm/Realm.h>

@interface CVMovieModel : RLMObject

@property NSString *ID;
@property NSString *title;
@property NSString *releaseDate;
@property NSString *overview;
@property NSString *posterPath;
@property NSNumber<RLMFloat> *voteAverage;
@property NSNumber<RLMDouble> *popularity;
@property BOOL isFav;

@end
