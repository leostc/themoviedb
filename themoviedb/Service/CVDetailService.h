//
//  CVDetailService.h
//  themoviedb
//
//  Created by HaoLi on 28/03/2018.
//  Copyright © 2018 curve. All rights reserved.
//

#import "CVBaseService.h"

@interface CVDetailService : CVBaseService

- (void)updateMovieFavStatusWithId:(NSString *)ID isFav:(BOOL)isFav;

@end
