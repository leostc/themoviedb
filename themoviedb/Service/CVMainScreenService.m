//
//  CVMainScreenService.m
//  themoviedb
//
//  Created by HaoLi on 28/03/2018.
//  Copyright © 2018 curve. All rights reserved.
//

#import "CVMainScreenService.h"
#import "CVMovieModel.h"
#import "AFNetworking.h"
#import "CVConfig.h"

@interface CVMainScreenService ()

@property (assign, nonatomic) NSInteger totalResults;
@property (assign, nonatomic) NSInteger totalPages;

@end

@implementation CVMainScreenService

- (instancetype)init {
    if (self = [super init]) {
        DLog(@"%@",[RLMRealmConfiguration defaultConfiguration].fileURL);
    }
    return self;
}

- (void)fetchDataWithPageIndex:(NSInteger)pageIndex
                       success:(CVDataRequestSuccessBlock)success
                       failure:(CVDataRequestFailureBlock)failure {
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    __weak __typeof(self) weakSelf = self;
    [manager GET:[CVConfig popularUrl]
      parameters:@{
                   @"api_key":APP_KEY,
                   @"page":@(pageIndex),
                   }
        progress:^(NSProgress * _Nonnull uploadProgress) {
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            __strong __typeof(weakSelf) strongSelf = weakSelf;
            if (![responseObject isKindOfClass:[NSDictionary class]]) {
                NSError *error = [NSError errorWithDomain:CV_HTTP_RESPONSE_ERROR_DOMAIN code:CV_HTTP_RESPONSE_ERROR_CODE_GENERAL userInfo:nil];
                if (failure) {
                    failure(error);
                }
                return;
            }
            NSArray *results = responseObject[@"results"];
            if (![results isKindOfClass:[NSArray class]]) {
                NSError *error = [NSError errorWithDomain:CV_HTTP_RESPONSE_ERROR_DOMAIN code:CV_HTTP_RESPONSE_ERROR_CODE_GENERAL userInfo:nil];
                if (failure) {
                    failure(error);
                }
                return;
            }
            strongSelf.totalResults = [responseObject[@"total_results"] integerValue];
            strongSelf.totalPages = [responseObject[@"total_pages"] integerValue];
            [strongSelf backgroundAddDataToDB:results];
            if (success) {
                success();
            }
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            if (failure) {
                failure(error);
            }
        }];
}

- (void)updateMovieFavStatusWithId:(NSString *)ID isFav:(BOOL)isFav {
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queue, ^{
        @autoreleasepool {
            RLMRealm *realm = [RLMRealm defaultRealm];
            [realm beginWriteTransaction];
            [CVMovieModel createOrUpdateInRealm:realm
                                      withValue:@{
                                                  @"ID": ID,
                                                  @"isFav": @(isFav),
                                                  }];
            [realm commitWriteTransaction];
        }
    });
}

- (void)backgroundAddDataToDB:(NSArray *)results {
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queue, ^{
        @autoreleasepool {
            RLMRealm *realm = [RLMRealm defaultRealm];
            [realm beginWriteTransaction];
            for (NSDictionary *result in results) {
                [CVMovieModel createOrUpdateInRealm:realm
                                          withValue:@{
                                                      @"ID": [NSString stringWithFormat:@"%@", result[@"id"]],
                                                      @"title": result[@"title"],
                                                      @"releaseDate": result[@"release_date"],
                                                      @"overview": result[@"overview"],
                                                      @"posterPath": result[@"poster_path"],
                                                      @"voteAverage": result[@"vote_average"],
                                                      @"popularity": result[@"popularity"],
                                                      }];
            }
            [realm commitWriteTransaction];
        }
    });
}

@end
