//
//  CVMainScreenService.h
//  themoviedb
//
//  Created by HaoLi on 28/03/2018.
//  Copyright © 2018 curve. All rights reserved.
//

#import "CVBaseService.h"

@class CVMovieModel;

typedef void(^CVDataRequestSuccessBlock)(void);
typedef void(^CVDataRequestFailureBlock)(NSError *error);

@interface CVMainScreenService : CVBaseService

- (void)fetchDataWithPageIndex:(NSInteger)pageIndex
                       success:(CVDataRequestSuccessBlock)success
                       failure:(CVDataRequestFailureBlock)failure;

- (void)updateMovieFavStatusWithId:(NSString *)ID isFav:(BOOL)isFav;

@end
