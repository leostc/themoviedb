//
//  CVDetailService.m
//  themoviedb
//
//  Created by HaoLi on 28/03/2018.
//  Copyright © 2018 curve. All rights reserved.
//

#import "CVDetailService.h"
#import "CVMovieModel.h"

@implementation CVDetailService

- (instancetype)init {
    if (self = [super init]) {
        
    }
    return self;
}

- (void)updateMovieFavStatusWithId:(NSString *)ID isFav:(BOOL)isFav {
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queue, ^{
        @autoreleasepool {
            RLMRealm *realm = [RLMRealm defaultRealm];
            [realm beginWriteTransaction];
            [CVMovieModel createOrUpdateInRealm:realm
                                      withValue:@{
                                                  @"ID": ID,
                                                  @"isFav": @(isFav),
                                                  }];
            [realm commitWriteTransaction];
        }
    });
}

@end
