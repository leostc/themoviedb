//
//  CVDetailViewController.m
//  themoviedb
//
//  Created by HaoLi on 28/03/2018.
//  Copyright © 2018 curve. All rights reserved.
//

#import "CVDetailViewController.h"
#import "CVMovieModel.h"
#import "UIImageView+WebCache.h"
#import "CVConfig.h"
#import "CVDetailService.h"

@interface CVDetailViewController ()

@property (strong, nonatomic) CVMovieModel *movieModel;
@property (strong, nonatomic) CVDetailService *detailService;
@property (weak, nonatomic) IBOutlet UIImageView *posterImageView;
@property (strong, nonatomic) UIButton *rightNavButton;

@end

@implementation CVDetailViewController

- (CVDetailViewController *)initWithModel:(CVMovieModel*)model {
    if (self = [super init]) {
        _movieModel = model;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupNavBar];
    [self setupView];
}

- (void)setupNavBar {
    self.navigationItem.title = self.movieModel.title;
}

- (void)setupView {
    [self.posterImageView sd_setImageWithURL:[NSURL URLWithString:[CVConfig posterUrlWithPath:self.movieModel.posterPath]]];
    
    self.rightNavButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.rightNavButton addTarget:self action:@selector(fabButtonClicked) forControlEvents:UIControlEventTouchDown];
    UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.rightNavButton];
    self.navigationItem.rightBarButtonItem = rightBarButtonItem;
    
    if (self.movieModel.isFav) {
        [self.rightNavButton setImage:[UIImage imageNamed:@"like"] forState:UIControlStateNormal];
    } else {
        [self.rightNavButton setImage:[UIImage imageNamed:@"unlike"] forState:UIControlStateNormal];
    }
}

- (void)fabButtonClicked {
    if (self.movieModel.isFav) {
        [self.rightNavButton setImage:[UIImage imageNamed:@"unlike"] forState:UIControlStateNormal];
    } else {
        [self.rightNavButton setImage:[UIImage imageNamed:@"like"] forState:UIControlStateNormal];
    }
    [self.detailService updateMovieFavStatusWithId:self.movieModel.ID isFav:!self.movieModel.isFav];
}

#pragma mark - getter

- (CVDetailService *)detailService {
    if (!_detailService) {
        _detailService = [[CVDetailService alloc] init];
    }
    return _detailService;
}

@end
