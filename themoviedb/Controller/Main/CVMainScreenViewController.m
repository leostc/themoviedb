//
//  CVMainScreenViewController.m
//  themoviedb
//
//  Created by HaoLi on 28/03/2018.
//  Copyright © 2018 curve. All rights reserved.
//

#import "CVMainScreenViewController.h"
#import "CVMainScreenTableViewCell.h"
#import "CVDetailViewController.h"
#import "CVMainScreenService.h"
#import "MJRefresh.h"
#import "CVConfig.h"
#import <Realm/Realm.h>

static NSInteger const CV_MAIN_SCREEN_TABLEVIEW_CELL_HEIGHT = 155;

@interface CVMainScreenViewController () <UITableViewDelegate, UITableViewDataSource, CVMainScreenTableViewCellDelegate>

@property (weak, nonatomic) IBOutlet UITableView *mainScreenTableView;
@property (assign, nonatomic) NSInteger pageIndex;
@property (strong, nonatomic) CVMainScreenService *mainScreenService;
@property (strong, nonatomic) RLMResults *models;
@property (strong, nonatomic) RLMNotificationToken *notification;

@end

@implementation CVMainScreenViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupData];
    [self setupNavBar];
    [self setupTableView];
}

- (void)setupData {
    self.models = [[CVMovieModel allObjects] sortedResultsUsingKeyPath:@"popularity" ascending:NO];
    
    __weak typeof(self) weakSelf = self;
    self.notification = [self.models addNotificationBlock:^(RLMResults *data, RLMCollectionChange *changes, NSError *error) {
        if (error) {
            DLog(@"Failed to open Realm on background worker: %@", error);
            return;
        }
        
        __strong __typeof(weakSelf) strongSelf = weakSelf;
        UITableView *tv = strongSelf.mainScreenTableView;
        // Initial run of the query will pass nil for the change information
        if (!changes) {
            [tv reloadData];
            return;
        }
        
        // changes is non-nil, so we just need to update the tableview
        [tv beginUpdates];
        [tv deleteRowsAtIndexPaths:[changes deletionsInSection:0] withRowAnimation:UITableViewRowAnimationAutomatic];
        [tv insertRowsAtIndexPaths:[changes insertionsInSection:0] withRowAnimation:UITableViewRowAnimationAutomatic];
        [tv reloadRowsAtIndexPaths:[changes modificationsInSection:0] withRowAnimation:UITableViewRowAnimationAutomatic];
        [tv endUpdates];
    }];
}

- (void)setupNavBar {
    self.navigationItem.title = @"Popular Movies";
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    self.navigationController.navigationBar.barTintColor = NAV_BAR_COLOR;
}

- (void)setupTableView {
    [self.mainScreenTableView registerNib:[UINib nibWithNibName:@"CVMainScreenTableViewCell" bundle:nil] forCellReuseIdentifier:@"CVMainScreenTableViewCell"];
    
    self.mainScreenTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadNewData)];
    
    self.mainScreenTableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreData)];
    
    // Begin to reload
    [self.mainScreenTableView.mj_header beginRefreshing];
}

- (void)loadNewData {
    self.pageIndex = 1;
    [self.mainScreenTableView reloadData];
    __weak __typeof(self) weakSelf = self;
    [self.mainScreenService fetchDataWithPageIndex:self.pageIndex
                                           success:^{
                                               __strong __typeof(weakSelf) strongSelf = weakSelf;
                                               [strongSelf.mainScreenTableView.mj_header endRefreshing];
                                           } failure:^(NSError *error) {
                                               __strong __typeof(weakSelf) strongSelf = weakSelf;
                                               [strongSelf.mainScreenTableView.mj_header endRefreshing];
                                           }];
}

- (void)loadMoreData {
    self.pageIndex++;
    
    if (self.pageIndex * POPULAR_MOVIE_PAGE_SIZE <= self.models.count) {
        UITableView *tv = self.mainScreenTableView;
        [tv setContentOffset:tv.contentOffset animated:NO];
        NSMutableArray *rows = [NSMutableArray array];
        for (NSInteger i = (self.pageIndex - 1) * POPULAR_MOVIE_PAGE_SIZE;
             i < self.pageIndex * POPULAR_MOVIE_PAGE_SIZE;
             i++) {
            [rows addObject:[NSIndexPath indexPathForRow:i inSection:0]];
        }
        [tv beginUpdates];
        [tv insertRowsAtIndexPaths:rows withRowAnimation:UITableViewRowAnimationAutomatic];
        [tv endUpdates];
    }
    
    __weak __typeof(self) weakSelf = self;
    [self.mainScreenService fetchDataWithPageIndex:self.pageIndex
                                           success:^{
                                               __strong __typeof(weakSelf) strongSelf = weakSelf;
                                               [strongSelf.mainScreenTableView.mj_footer endRefreshing];
                                           } failure:^(NSError *error) {
                                               __strong __typeof(weakSelf) strongSelf = weakSelf;
                                               [strongSelf.mainScreenTableView.mj_footer endRefreshing];
                                           }];
}

#pragma mark - UITableView

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    NSInteger row = indexPath.row;
    CVMainScreenTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CVMainScreenTableViewCell" forIndexPath:indexPath];
    cell.delegate = self;
    [cell loadViewWitModel:self.models[row]];
    return cell;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return MIN(self.pageIndex * POPULAR_MOVIE_PAGE_SIZE, self.models.count);
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return CV_MAIN_SCREEN_TABLEVIEW_CELL_HEIGHT;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    CVDetailViewController *detailVC = [[CVDetailViewController alloc] initWithModel:self.models[indexPath.row]];
    [self.navigationController pushViewController:detailVC animated:YES];
}

#pragma mark - CVMainScreenTableViewCellDelegate

- (void)updateFavStatus:(CVMovieModel *)model {
    [self.mainScreenService updateMovieFavStatusWithId:model.ID isFav:!model.isFav];
}

#pragma mark - getter

- (CVMainScreenService *)mainScreenService {
    if (!_mainScreenService) {
        _mainScreenService = [[CVMainScreenService alloc] init];
    }
    return _mainScreenService;
}

@end
