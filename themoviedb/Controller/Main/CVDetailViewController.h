//
//  CVDetailViewController.h
//  themoviedb
//
//  Created by HaoLi on 28/03/2018.
//  Copyright © 2018 curve. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CVMovieModel;

@interface CVDetailViewController : UIViewController

- (CVDetailViewController *)initWithModel:(CVMovieModel*)model;

@end
