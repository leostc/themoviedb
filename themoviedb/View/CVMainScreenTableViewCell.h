//
//  MainScreenTableViewCell.h
//  themoviedb
//
//  Created by HaoLi on 28/03/2018.
//  Copyright © 2018 curve. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CVMovieModel.h"

@protocol CVMainScreenTableViewCellDelegate <NSObject>

- (void)updateFavStatus:(CVMovieModel *)model;

@end

@interface CVMainScreenTableViewCell : UITableViewCell

@property (weak, nonatomic) id<CVMainScreenTableViewCellDelegate> delegate;

- (void)loadViewWitModel:(CVMovieModel *)model;

@end
