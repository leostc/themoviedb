//
//  MainScreenTableViewCell.m
//  themoviedb
//
//  Created by HaoLi on 28/03/2018.
//  Copyright © 2018 curve. All rights reserved.
//

#import "CVMainScreenTableViewCell.h"
#import "UIImageView+WebCache.h"
#import "CVConfig.h"

@interface CVMainScreenTableViewCell()

@property (weak, nonatomic) IBOutlet UIImageView *movieImageView;
@property (weak, nonatomic) IBOutlet UILabel *movieTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *detialLabel;
@property (weak, nonatomic) IBOutlet UILabel *voteLabel;
@property (weak, nonatomic) IBOutlet UIButton *favButton;
@property (strong, nonatomic) CVMovieModel *model;

@end

@implementation CVMainScreenTableViewCell

- (void)loadViewWitModel:(CVMovieModel *)model {
    self.model = model;
    [self.movieImageView sd_setImageWithURL:[NSURL URLWithString:[CVConfig posterUrlWithPath:model.posterPath]]];
    self.movieTitleLabel.text = model.title;
    self.timeLabel.text = model.releaseDate;
    self.detialLabel.text = model.overview;
    
    self.voteLabel.text = [NSString stringWithFormat:@"%.1f", model.voteAverage.floatValue];
    
    CGFloat vote = model.voteAverage.floatValue;
    if (vote >= 7.0) {
        self.voteLabel.textColor = [UIColor greenColor];
    } else if (vote >= 4.0) {
        self.voteLabel.textColor = [UIColor orangeColor];
    } else {
        self.voteLabel.textColor = [UIColor redColor];
    }
    
    if (model.isFav) {
        [self.favButton setImage:[UIImage imageNamed:@"like"] forState:UIControlStateNormal];
    } else {
        [self.favButton setImage:[UIImage imageNamed:@"unlike"] forState:UIControlStateNormal];
    }
}

- (IBAction)likeButtonClicked:(id)sender {
    if (self.model.isFav) {
        [self.favButton setImage:[UIImage imageNamed:@"unlike"] forState:UIControlStateNormal];
    } else {
        [self.favButton setImage:[UIImage imageNamed:@"like"] forState:UIControlStateNormal];
    }
    if ([self.delegate respondsToSelector:@selector(updateFavStatus:)]) {
        [self.delegate updateFavStatus:self.model];
    }
}

@end
