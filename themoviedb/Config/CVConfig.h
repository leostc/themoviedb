//
//  CVConfig.h
//  themoviedb
//
//  Created by HaoLi on 28/03/2018.
//  Copyright © 2018 curve. All rights reserved.
//

#import <Foundation/Foundation.h>

#ifdef DEBUG
#define DLog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);
#else
#define DLog(...)
#endif

#define CV_COLOR(r,g,b) [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:(1)]

#define NAV_BAR_COLOR CV_COLOR(13, 29, 35)

extern NSInteger const POPULAR_MOVIE_PAGE_SIZE;

extern NSString * const APP_KEY;

extern NSString * const CV_HTTP_RESPONSE_ERROR_DOMAIN;

typedef NS_ENUM(NSUInteger, CV_HTTP_RESPONSE_ERROR_CODE) {
    CV_HTTP_RESPONSE_ERROR_CODE_GENERAL = 1001,
};

@interface CVConfig : NSObject

+ (NSInteger)screenHeight;
+ (NSInteger)screenWidth;

+ (NSString *)popularUrl;
+ (NSString *)posterUrlWithPath:(NSString *)path;

@end
