//
//  CVConfig.m
//  themoviedb
//
//  Created by HaoLi on 28/03/2018.
//  Copyright © 2018 curve. All rights reserved.
//

#import "CVConfig.h"
#import <UIKit/UIKit.h>

NSInteger const POPULAR_MOVIE_PAGE_SIZE = 20;

NSString * const APP_KEY = @"8b1d6336b09621d7b8fb0108865946c0";

NSString * const CV_HTTP_RESPONSE_ERROR_DOMAIN = @"CV_HTTP_RESPONSE_ERROR_DOMAIN";

@implementation CVConfig

+ (NSInteger)screenHeight {
    return [UIScreen mainScreen].bounds.size.height;
}

+ (NSInteger)screenWidth {
    return [UIScreen mainScreen].bounds.size.width;
}

+ (NSString *)popularUrl {
    return @"https://api.themoviedb.org/3/movie/popular";
}

+ (NSString *)posterUrlWithPath:(NSString *)path {
    return [NSString stringWithFormat:@"http://image.tmdb.org/t/p/w500%@", path];
}

@end
